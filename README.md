# low-code 简易可视化拖拽生成页面

## 组件扩展
1. 在src/components下增加parser-组件名.tsx来转换自己的插件。参数只需要传进来jsonScheme
2. 在src/components/index.ts下增加解析器（此处可优化成自动导入）

```javascript
// jsonScheme格式
jsonScheme = {
  "page": {
    "type": "Container", //根容器
    "children": [
      {
        "type": "Container", // 类型
        "props":{} // 传到组件上的参数
        "children": [  // 子类
          {
            "type": "CButton",
          },
          { 
            "type": "CInput" 
          }
        ]
      }
    ]
  }
}
```
### 配置面板扩展
待优化，目前需要每种类型搞一个tsx文件，后续会优化成只需要写json传入就渲染指定的。

### todo-list
 1. √物料堆

 2. √画布/渲染引擎

 3. √配置面板

 4. √拖拽物料堆到画布

 5. x画布内组件的拖拽

 6. x拖拽时位置高亮

 7. x选中时边框高亮

