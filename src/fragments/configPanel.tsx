import {FC, useContext, useMemo} from "react";
import { EditorContext } from '../context/editorContext';
import { configList } from '../configs';


const ConfigPanel:FC = ()=>{
   //useContext值变化时，hook会触发重新渲染
  //useMemo会在第一次执行的时候调用，其余时候数据变化时,才会调用
  const { selectedType } = useContext(EditorContext);

  const renderSelectedPanel = useMemo(()=>{
    const ConfigMod = configList[selectedType as "Container"];
    return <ConfigMod />
  },[selectedType])

  const renderEmptyPanel = useMemo(()=>{
    return <div>not selected</div>
  },[])
  return selectedType ? renderSelectedPanel : renderEmptyPanel;
}

export default ConfigPanel;