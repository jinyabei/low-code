import React from 'react';
import ReactDOM from 'react-dom/client';
import MainPage from './pages/mainPage';
import './virtual:windi.css';
//引入acro-design样式
import "@arco-design/web-react/dist/css/arco.css";

import {Provider}from 'react-redux'
import {store} from './redux/store'
import { EditorContextProvider } from './context/editorContext';

const root = ReactDOM.createRoot(document.getElementById('root') as Element | DocumentFragment);
root.render(
  <Provider store={store}>
    <EditorContextProvider>
      <MainPage />
    </EditorContextProvider>
  </Provider>
);
