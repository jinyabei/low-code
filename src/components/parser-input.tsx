import { FC, PropsWithChildren } from "react";
import CInput from "./cInput";

const ParserInput: FC<PropsWithChildren<{}>> = (props) => {
  return <CInput>{props.children}</CInput>;
};

export default ParserInput;