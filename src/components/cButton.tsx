import { FC, PropsWithChildren } from "react";
import { Button } from "@arco-design/web-react";

interface IProps {
  handleOnClick?:(e:Event)=>void;
}
const CButton :FC<PropsWithChildren<IProps>> = (props)=>{
  const {handleOnClick} = props;
  return <Button onClick={handleOnClick} {...props}>{props.children}</Button>;
}

export default CButton;