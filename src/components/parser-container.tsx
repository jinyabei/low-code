import React, { FC,DragEvent,PropsWithChildren, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { addNodeIntoContainer } from '../redux/reducers/editSlice';
import Container from './Container';
import { EditorContext } from '../context/editorContext';


interface IProps {
  jsonScheme: Record<string, any>;
  addNode?: string;
}

const ParserContainer: FC<PropsWithChildren<IProps>> = (props) => {
  const { jsonScheme, addNode } = props;
  const dispatch = useDispatch();

  const { updateSelectedType } = useContext(EditorContext);

  const handleDrop = (e: DragEvent<HTMLDivElement>) => {
    e.stopPropagation();
    const { componentId } = jsonScheme;

    dispatch(
      addNodeIntoContainer({
        addType: addNode,
        targetComponentId: componentId,
      }),
    );
  };

  const handleDragOver = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault();
  };

  const handleOnClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    e.stopPropagation();
    const {type} = jsonScheme;
    updateSelectedType(type);
  };

  return (
    <Container
      {...props}
      handleDrop={handleDrop}
      handleDragOver={handleDragOver}
      handleOnClick={handleOnClick}
    >
      {props.children}
    </Container>
  );
};

export default ParserContainer;
