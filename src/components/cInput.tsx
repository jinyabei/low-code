import { FC, PropsWithChildren } from "react";
import { Input } from "@arco-design/web-react";

const CInput:FC<PropsWithChildren<{}>> = ()=>{
  return <Input />
}

export default CInput;