import { Dispatch, FC, PropsWithChildren, SetStateAction, createContext, useState } from 'react';

interface IContext {
  addType: string;
  updateAddType: Dispatch<SetStateAction<string>>;
  selectedType: string;
  updateSelectedType: Dispatch<SetStateAction<string>>;
  selectedCompId: string;
  updateSelectedCompId: Dispatch<SetStateAction<string>>;
}
const defaultValue = {
  addType: '',
  updateAddType: () => {},
  selectedType: '',
  updateSelectedType: () => {},
  selectedCompId: '',
  updateSelectedCompId: () => {},
};

export const EditorContext = createContext<IContext>(defaultValue);

export const EditorContextProvider: FC<PropsWithChildren<{}>> = (props) => {
  const [addType, updateAddType] = useState<string>('');
  const [selectedType, updateSelectedType] = useState<string>('');
  const [selectedCompId, updateSelectedCompId] = useState<string>("");

  const value = {
    addType,
    updateAddType,
    selectedType,
    updateSelectedType,
    selectedCompId,
    updateSelectedCompId,
  };
  return (
    <EditorContext.Provider value={value}>
      {props.children}
    </EditorContext.Provider>
  );
};
