//随机生成组件id
export const getCompId = ()=>{
  return Number(
    (Math.random()*9+1*10000).toFixed(0)+Date.now()
  ).toString(36)
}

//根据组件id找到组件
export const findCompById = (tree:Record<string,any>,compId:string)=>{
  const current = tree;
  const queue = new Array<Record<string,any>>();
  queue.push(current);
  while(queue.length){
    const head = queue[queue.length-1]
    if(head.componentId === compId){
      return head
    }
    queue.pop()
    if(head.children&&head.children.length){
      queue.push(...head.children)
    }
  }
  return null
}